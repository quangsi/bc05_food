import { deleteFoodByID, getAllFood } from "../../service/service.js";
import { renderFoodList } from "./controller-v2.js";

getAllFood()
  .then((res) => {
    renderFoodList(res.data);
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });

function xoaMonAn(id) {
  deleteFoodByID(id)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
  console.log("id: ", id);
}

window.xoaMonAn = xoaMonAn;
