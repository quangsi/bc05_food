export const renderFoodList = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let { ten, loai, gia, tinhTrang, ma, phanTramKM } = item;
    contentHTML += `<tr>
   
   <td>${ma}</td>
   <td>${ten}</td>
   <td>${loai ? "Mặn" : "Chay"}</td>
   <td>${gia}</td>
   <td>${phanTramKM} %</td>
   <td>0</td>
   <td>${tinhTrang ? "Còn" : "Hết"}</td>
   <td>
   <button onclick="xoaMonAn(${ma})" class="btn btn-danger">Xoá</button>
   <button class="btn btn-secondary">Sửa</button>
   </td>
   </tr>`;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};

// loại : true=> mặn

// tình trang : true => còn
