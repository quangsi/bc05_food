import { layThongTinTuForm } from "./controller-v1.js";

import hieThiThongTin from "./controller-v1.js";
import { MonAn } from "../../models/v1/monAnModel-v1.js";
// thêm món ăn
document.getElementById("btnThemMon").addEventListener("click", function () {
  let data = layThongTinTuForm();
  let { ma, ten, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } = data;
  let monAn = new MonAn(
    ma,
    ten,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  hieThiThongTin(monAn);
  console.log("monAn: ", monAn);
});
