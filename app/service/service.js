let BASE_URL = "https://635f4b15ca0fe3c21a991c9b.mockapi.io";

export let getAllFood = () => {
  return axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  });
};

export let deleteFoodByID = (id) => {
  return axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  });
};
